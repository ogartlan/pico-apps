#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/float.h" // Required for using single-precision variables.
#include "pico/double.h" // Required for using double-precision variables.
#include "pico/multicore.h" // Required for using multiple cores on the RP2040.
/**
* @brief This function acts as the main entry-point for core #1.
* A function pointer is passed in via the FIFO with one
* incoming int32_t used as a parameter. The function will
* provide an int32_t return value by pushing it back on
* the FIFO, which also indicates that the result is ready.
*/
void core1_entry() {
 while (1) {
 // Function pointer is passed to us via the FIFO
    // We have one incoming int32_t as a parameter, and will provide an
    // int32_t return value by simply pushing it back on the FIFO
    // which also indicates the result is ready.
 int32_t (*func)() = (int32_t(*)()) multicore_fifo_pop_blocking(); 
 int32_t p = multicore_fifo_pop_blocking();
 int32_t result = (*func)(p);
 multicore_fifo_push_blocking(result);
 }
}
// Main code entry point for core0.
int main() {
 const int ITER_MAX = 100000; //num of iterations
 stdio_init_all();
 multicore_launch_core1(core1_entry);   //starts going into core 1?

 multicore_fifo_push_blocking((uintptr_t) &factorial);
 multicore_fifo_push_blocking(TEST_NUM);

 // Code for sequential run goes here…

 // Take snapshot of timer and store
 // Run the single-precision Wallis approximation
 // Run the double-precision Wallis approximation
 // Take snapshot of timer and store
 // Display time taken for application to run in sequential mode
 // Code for parallel run goes here…
 // Take snapshot of timer and store
 // Run the single-precision Wallis approximation on one core
 // Run the double-precision Wallis approximation on the other core
 // Take snapshot of timer and store
 // Display time taken for application to run in parallel mode
 return 0;
}
