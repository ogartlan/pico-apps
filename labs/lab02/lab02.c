//#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/float.h" 
#include "pico/double.h"


/**
 * @brief EXAMPLE - HELLO_C
 *        Simple example to initialise the IOs and then 
 *        print a "Hello World!" message to the console.
 * 
 * @return int  Application return code (zero for success).
 */
int main() {


/*
#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
    stdio_init_all();
#endif
*/

    const float N = 100000;
    double sing_err;
    double doub_err;
    double true_pi = 3.14159265359;

    float q = 2;

    float sing = ((q/(q - 1)) * (q/(q + 1))); /**
     * @brief Initialise and then run the Wallis Product Algorithm
     * This first line initialises it by producing the first term
     * After this the for loop can generate N - 1 more iterations to allow N in total.
     * The same approach is used for doub underneath. float q is needed to ensure float division is performed.
     */
    for(float n = 2; n < N+1; n++){
        sing = (((2*n)/((2*n) - 1)) * ((2*n)/((2*n) + 1))) * sing;
    }
    sing = 2 * sing;

    double doub = ((q/(q - 1)) * (q/(q + 1)));
    for(double n = 2; n < N+1; n++){
        doub = (((2*n)/((2*n) - 1)) * ((2*n)/((2*n) + 1))) * doub;
    }
    doub = 2 * doub;

    sing_err = sing - true_pi;
    doub_err = doub - true_pi;


    // Prints a console message to inform user what's going on.
    printf("Single-precision pi: %f \n", sing);
    printf("With total approximation error: %f \n\n", sing_err);

    printf("Double-precision pi: %f \n", doub);
    printf("With total approximation error: %f \n\n", doub_err);

    // Returning zero indicates everything went okay.
    return 0;
}
